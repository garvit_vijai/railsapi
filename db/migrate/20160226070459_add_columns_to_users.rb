class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :type, :string
    add_column :users, :contact_no, :string
    add_reference :users, :restaurant, index: true
    add_foreign_key :users, :restaurants
  end
end
