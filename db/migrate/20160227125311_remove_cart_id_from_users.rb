class RemoveCartIdFromUsers < ActiveRecord::Migration
  def change
    remove_reference :users, :cart, index: true
    remove_foreign_key :users, :carts
  end
end
