class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :serve_time
      t.float :cost
      t.references :restaurant, index: true

      t.timestamps null: false
    end
    add_foreign_key :items, :restaurants
  end
end
