class Lib::Product
  attr_accessor :product

  def initialize(product)
    @product = product
  end

  def score(score_for='popularity')
    likes_count = product.likes_count
    comments_count = product.comments_count rescue 0 # no commenting as of now

    ttl = Metadata.find_by(meta_type: score_for, name: 'ttl').value.to_f # in hours
    gravity = Metadata.find_by(meta_type: score_for, name: 'gravity').value.to_f

    num = (likes_count + comments_count).to_f
    den = (time_since_published + ttl).to_f

    res = ((num/den)**gravity).round(4)
  end

  private

  def time_since_published
    seconds = ((Time.now - product.created_at).to_f)
    minutes = seconds/60.0
    hours = minutes/60.0
    days = hours/24.0
    days
  end

end
