class LookUp::Base
  attr_accessor :item_id

  def initialize(item_id)
    @item_id = item_id
  end

  def large_image_url
    raise I18n.t('exceptions.method_override_required')
  end

  def medium_image_url
    raise I18n.t('exceptions.method_override_required')
  end

  def small_image_url
    raise I18n.t('exceptions.method_override_required')
  end

  def manufacturer
    raise I18n.t('exceptions.method_override_required')
  end

  def product_group
    raise I18n.t('exceptions.method_override_required')
  end

  def title
    raise I18n.t('exceptions.method_override_required')
  end

  def currency_code
    raise I18n.t('exceptions.method_override_required')
  end

  def amount
    raise I18n.t('exceptions.method_override_required')
  end

  def formatted_price
    raise I18n.t('exceptions.method_override_required')
  end
  
  def errors
    raise I18n.t('exceptions.method_override_required')
  end

  def is_valid?
    raise I18n.t('exceptions.method_override_required')
  end
end