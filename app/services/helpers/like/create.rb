class Helpers::Like::Create
  attr_accessor :user

  def initialize(user=nil)
    @user = user
  end

  def using_likeable(likeable)
    like = user.like(likeable)
    if like.likeable_type.is? 'Product'
      Helpers::Product::Scores.new(likeable).update 
    end
    like
  end
end