class Helpers::Product::Create
  attr_accessor :user

  def initialize(user=nil)
    @user = user
  end

  def from_url(product_url, save_item=true)
    item_id = item_id(product_url)
    product = Product.find_by(item_id: item_id)
    if product.present?
      return product
    end
    # find details of the product from amazon
    look_up = LookUp::Amazon.new(item_id)

    product = ::Product.new(url: product_url)
    if look_up.is_valid?
      build_product_from_look_up(look_up, product)
      product.remote_image_url = product.large_image_url

      if save_item and product.save
        product.reload.add_to_category(Category.find_or_create_by(name: look_up.product_group))

        # save product images from our servers
        product.update(
          large_image_url: product.image.url,
          medium_image_url: product.image.medium.url,
          small_image_url: product.image.thumb.url
        )
      end
    else
      product.errors[:product_url] << "Product url is not valid."
    end
    product
  end

  private

  # extract item id if from product url
  def item_id(product_url)
    product_url.match('/([A-Z0-9]{10})')[1].strip rescue ''
  end

  def build_product_from_look_up(look_up, product)
    # set the image urls

    product.large_image_url = look_up.maybe.large_image_url
    product.medium_image_url = look_up.maybe.medium_image_url
    product.small_image_url = look_up.maybe.small_image_url

    # set item id
    product.item_id = look_up.item_id

    # set brand details
    product.manufacturer = look_up.manufacturer
    product.product_group = look_up.product_group
    product.title = look_up.title

    # set pricing details
    product.currency_code = look_up.maybe.currency_code
    amount = look_up.amount.to_f
    product.amount = (amount <= 0) ? nil : amount
    product.formatted_price = look_up.maybe.formatted_price
    product.url = look_up.url

    # associate the user
    product.user = user
  end
end
