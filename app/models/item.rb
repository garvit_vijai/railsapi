class Item < ActiveRecord::Base
  belongs_to :restaurant

  validates :name, presence: true , length: {in: 1..50 }
  validates :serve_time ,  presence: true ,:format => { :with => /\A[a-zA-Z ]+\z/ }
  validates :serve_time , inclusion: { in: %w(breakfast lunch dinner) }
  validates :cost, presence: true , :numericality => { :only_integer =>true , 
  																										:greater_than_or_equal_to => 1}


end
