class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  def worker?
  	self.type == "Worker"
  end

  def admin?
  	self.type == "Admin"
  end

  def customer?
    self.type == "Customer"
  end
  
end
