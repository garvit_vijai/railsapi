class Customer < User
	has_many :carts, dependent: :destroy, 
															 foreign_key: "user_id"
  has_many :orders, dependent: :destroy, 
  														 foreign_key: "user_id"


  def active_cart
    self.carts.last 
  end

end