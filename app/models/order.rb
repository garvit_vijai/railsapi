class Order < ActiveRecord::Base
  belongs_to :customer, class_name: "User", 
                        foreign_key: "user_id"
                        
  belongs_to :restaurant
  belongs_to :cart

  validates :amount ,presence: true , :numericality => { :greater_than => 50 } 
 
  def accept
    self.update(status: "accepted")
  end
  def reject
    self.update(status: "rejected")
  end
  
  scope :pending_orders, 	->{ where(status: 'pending').limit(10).order(created_at: :desc)}
  
  delegate :cart_items , :to => :cart
end
