class Cart < ActiveRecord::Base
	acts_as_shopping_cart_using :cart_item
	
	belongs_to :customer,class_name: "User",
											foreign_key: "user_id"

end
