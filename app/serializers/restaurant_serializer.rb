class RestaurantSerializer < ActiveModel::Serializer
  attributes :id,
  					 :name,
  					 :location,
  					 :opening_hour,
  					 :closing_hour,
  					 :items

end
