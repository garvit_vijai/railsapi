class Api::V1::Admin::ItemsController < Api::V1::Admin::ApplicationController
	
	before_action :set_restaurant_items, only: [:index,:show,:create,:destroy,:update]

	def index
		serializer_responder(@restaurant_items,each_seraializer: ItemSerializer)
	end

	def show
		serializer_responder(@restaurant_items.find(params[:id]),serializer: ItemSerializer)
	end

	def create
		#byebug
		serializer_responder(@restaurant_items.create(pass_items), serializer: ItemSerializer)
	end
  
  def update
  	item = @restaurant_items.find(params[:id])
  	item.update(pass_items)
  	serializer_responder(item.reload, serializer: ItemSerializer)
  end

	def destroy
		serializer_responder(@restaurant_items.find(params[:id]).destroy)
	end


	private

	 def set_restaurant_items
	 	@restaurant_items = current_user.restaurant.items
	 end

	 def pass_items
	 	params.require(:item).permit(:name,:serve_time,:cost)
	 end
	
end
