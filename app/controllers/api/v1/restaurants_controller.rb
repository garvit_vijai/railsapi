class Api::V1::RestaurantsController < Api::V1::ApplicationController
 
 def index
  serializer_responder(Restaurant.all, each_serializer: RestaurantSerializer)
 end

 def show
 	serializer_responder(Restaurant.find(params[:id]), serializer: RestaurantSerializer)
 end
 
end
